/***
ASSIGNMENT-1,
QUESTION-5,
NAME-B GOPI REDDY,
COURES-e-DESD,
MODULE-EMBEDDED WITH C,
***/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
typedef struct Employee{
    int id;
    char name[ 30 ];
    float price;
    int quntity;
}item_t;

void accept_record( item_t *ptr ){
    printf("id    :   ");
    scanf("%d", &ptr->id);
    printf("Name    :   ");
    scanf("%s", ptr->name);
    printf("price    :   ");
    scanf("%f", &ptr->price);
    printf("quntity    :   ");
    scanf("%d", &ptr->quntity);
}
void print_record( item_t *ptr ){
    printf("%d      %s        %1f     %d \n",ptr->id, ptr->name, ptr->price, ptr->quntity);
}
void add( const char *path){   
    FILE *stream = fopen( path, "ab"); 
    if( stream != NULL ){
        item_t emp;
        accept_record( &emp );
        fwrite( &emp, sizeof(item_t), 1, stream );
        fclose( stream );
    }else
        fprintf(stderr, "File write operation is failed.\n");
}
void edit( const char *path, int id, float price ){   
    FILE *stream = fopen( path, "rb+"); 
    if( stream != NULL ){
        item_t emp;
       while( fread( &emp, sizeof(item_t), 1, stream ) == 1 ) {
           if( emp.id == id ){
               emp.price = price;
               fseek( stream, -1 * sizeof( item_t ), SEEK_CUR );
                fwrite( &emp, sizeof(item_t), 1, stream );
               break;
           }
       }
        fclose( stream );
    }else
        fprintf(stderr, "File write operation is failed.\n");
}
void find( const char *path){   
    FILE *stream = fopen( path, "rb");
    if( stream != NULL ){
        item_t emp;
        srand(time(0));
        fseek( stream, 2 * sizeof( item_t), SEEK_CUR );
        fread( &emp, sizeof(item_t), 1, stream );
        printf("%d",rand());
        print_record( &emp);
        fclose( stream );
    }else
        fprintf(stderr, "File read operation is failed.\n");
}
void delete( const char *path, int id ){   
    FILE *stream = fopen( path, "rb"); 
    FILE *fp = fopen( "Temp.dat", "wb"); 
    if( stream != NULL ){
        item_t emp;
        while( fread( &emp, sizeof(item_t), 1, stream ) == 1 ) {
            if( emp.id != id)
                fwrite( &emp, sizeof(item_t), 1, fp );
        }
        fclose( fp );
        fclose( stream );
        remove( path );
        rename( "Temp.dat", path);
    }else
        fprintf(stderr, "File write operation is failed.\n");
}
void display( const char *path){    
    FILE *stream = fopen( path, "rb");
    if( stream != NULL ){
        item_t emp;
        printf("________________________________\n");
        printf("Id      Name      price     quntity\n");
        printf("________________________________\n");
        while( fread( &emp, sizeof(item_t), 1, stream ) == 1 ) 
            print_record( &emp);
        fclose( stream );
    }else
        fprintf(stderr, "File read operation is failed.\n");
}
int menu_list( void ){
    int choice;
    printf("0.Exit\n");
    printf("1.add\n");
    printf("2.display\n");
    printf("3.edit\n");
    printf("4.Delete\n");
    printf("5.find\n");
    printf("Enter choice    :   ");
    scanf("%d", &choice);
    return choice;
}
int main( void ){
    int choice;
    while( ( choice = menu_list( ) ) != 0 ){
        switch( choice ){
        case 1:
            add("File.dat");
            break;
        case 2:
            display("File.dat");
            break;
        case 3:
            edit( "File.dat", 0,0);
            break;
        case 4:
            delete( "File.dat",50);
            break;
        case 5:
            find( "File.dat");
            break;    

        }
    }
    return 0;
}
